EESchema Schematic File Version 4
LIBS:SmartBoard - Grid-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS165 U3
U 1 1 60817CF2
P 9050 5400
F 0 "U3" H 9050 5400 50  0000 C CNN
F 1 "74LS165" H 9050 6387 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 9050 5400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS165" H 9050 5400 50  0001 C CNN
	1    9050 5400
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS165 U4
U 1 1 60817D3B
P 8200 2650
F 0 "U4" H 8200 2650 50  0000 C CNN
F 1 "74LS165" H 8200 3637 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 8200 2650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS165" H 8200 2650 50  0001 C CNN
	1    8200 2650
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS125 U1
U 5 1 60817E41
P 2800 3250
F 0 "U1" H 3030 3296 50  0000 L CNN
F 1 "74LS125" H 3030 3205 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 2800 3250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 2800 3250 50  0001 C CNN
	5    2800 3250
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74LS125 U1
U 1 1 60817EA8
P 2800 2150
F 0 "U1" H 2800 2467 50  0000 C CNN
F 1 "74LS125" H 2800 2376 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 2800 2150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 2800 2150 50  0001 C CNN
	1    2800 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 60817F45
P 6600 2150
F 0 "R1" H 6670 2196 50  0000 L CNN
F 1 "10k" H 6670 2105 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 2150 50  0001 C CNN
F 3 "~" H 6600 2150 50  0001 C CNN
	1    6600 2150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 60817FD1
P 6600 2250
F 0 "R2" H 6670 2296 50  0000 L CNN
F 1 "10k" H 6670 2205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 2250 50  0001 C CNN
F 3 "~" H 6600 2250 50  0001 C CNN
	1    6600 2250
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 6081802B
P 6600 2350
F 0 "R3" H 6670 2396 50  0000 L CNN
F 1 "10k" H 6670 2305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 2350 50  0001 C CNN
F 3 "~" H 6600 2350 50  0001 C CNN
	1    6600 2350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R4
U 1 1 6081804F
P 6600 2450
F 0 "R4" H 6670 2496 50  0000 L CNN
F 1 "10k" H 6670 2405 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 2450 50  0001 C CNN
F 3 "~" H 6600 2450 50  0001 C CNN
	1    6600 2450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 60818073
P 6600 2550
F 0 "R5" H 6670 2596 50  0000 L CNN
F 1 "10k" H 6670 2505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 2550 50  0001 C CNN
F 3 "~" H 6600 2550 50  0001 C CNN
	1    6600 2550
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R6
U 1 1 6081809B
P 6600 2650
F 0 "R6" H 6670 2696 50  0000 L CNN
F 1 "10k" H 6670 2605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 2650 50  0001 C CNN
F 3 "~" H 6600 2650 50  0001 C CNN
	1    6600 2650
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R7
U 1 1 608180C3
P 6600 2750
F 0 "R7" H 6670 2796 50  0000 L CNN
F 1 "10k" H 6670 2705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 2750 50  0001 C CNN
F 3 "~" H 6600 2750 50  0001 C CNN
	1    6600 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R8
U 1 1 608180F1
P 6600 2850
F 0 "R8" H 6670 2896 50  0000 L CNN
F 1 "10k" H 6670 2805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 2850 50  0001 C CNN
F 3 "~" H 6600 2850 50  0001 C CNN
	1    6600 2850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R9
U 1 1 60818457
P 6600 4900
F 0 "R9" H 6670 4946 50  0000 L CNN
F 1 "10k" H 6670 4855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 4900 50  0001 C CNN
F 3 "~" H 6600 4900 50  0001 C CNN
	1    6600 4900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R10
U 1 1 6081845E
P 6600 5000
F 0 "R10" H 6670 5046 50  0000 L CNN
F 1 "10k" H 6670 4955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 5000 50  0001 C CNN
F 3 "~" H 6600 5000 50  0001 C CNN
	1    6600 5000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R11
U 1 1 60818465
P 6600 5100
F 0 "R11" H 6670 5146 50  0000 L CNN
F 1 "10k" H 6670 5055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 5100 50  0001 C CNN
F 3 "~" H 6600 5100 50  0001 C CNN
	1    6600 5100
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R12
U 1 1 6081846C
P 6600 5200
F 0 "R12" H 6670 5246 50  0000 L CNN
F 1 "10k" H 6670 5155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 5200 50  0001 C CNN
F 3 "~" H 6600 5200 50  0001 C CNN
	1    6600 5200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R13
U 1 1 60818473
P 6600 5300
F 0 "R13" H 6670 5346 50  0000 L CNN
F 1 "10k" H 6670 5255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 5300 50  0001 C CNN
F 3 "~" H 6600 5300 50  0001 C CNN
	1    6600 5300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R14
U 1 1 6081847A
P 6600 5400
F 0 "R14" H 6670 5446 50  0000 L CNN
F 1 "10k" H 6670 5355 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 5400 50  0001 C CNN
F 3 "~" H 6600 5400 50  0001 C CNN
	1    6600 5400
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R15
U 1 1 60818481
P 6600 5500
F 0 "R15" H 6670 5546 50  0000 L CNN
F 1 "10k" H 6670 5455 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 5500 50  0001 C CNN
F 3 "~" H 6600 5500 50  0001 C CNN
	1    6600 5500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R16
U 1 1 60818488
P 6600 5600
F 0 "R16" H 6670 5646 50  0000 L CNN
F 1 "10k" H 6670 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6530 5600 50  0001 C CNN
F 3 "~" H 6600 5600 50  0001 C CNN
	1    6600 5600
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x16_Male J3
U 1 1 60819011
P 10100 2650
F 0 "J3" H 10206 3528 50  0000 C CNN
F 1 "Ground" H 10206 3437 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 10100 2650 50  0001 C CNN
F 3 "~" H 10100 2650 50  0001 C CNN
	1    10100 2650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x16_Male J4
U 1 1 60819075
P 10500 2650
F 0 "J4" H 10606 3528 50  0000 C CNN
F 1 "Power" H 10606 3437 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 10500 2650 50  0001 C CNN
F 3 "~" H 10500 2650 50  0001 C CNN
	1    10500 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 1950 10300 2050
Connection ~ 10300 2050
Wire Wire Line
	10300 2050 10300 2150
Connection ~ 10300 2150
Wire Wire Line
	10300 2150 10300 2250
Connection ~ 10300 2250
Wire Wire Line
	10300 2250 10300 2350
Connection ~ 10300 2350
Wire Wire Line
	10300 2350 10300 2450
Connection ~ 10300 2450
Wire Wire Line
	10300 2450 10300 2550
Connection ~ 10300 2550
Wire Wire Line
	10300 2550 10300 2650
Connection ~ 10300 2650
Wire Wire Line
	10300 2650 10300 2750
Connection ~ 10300 2750
Wire Wire Line
	10300 2750 10300 2850
Connection ~ 10300 2850
Wire Wire Line
	10300 2850 10300 2950
Connection ~ 10300 2950
Wire Wire Line
	10300 2950 10300 3050
Connection ~ 10300 3050
Wire Wire Line
	10300 3050 10300 3150
Connection ~ 10300 3150
Wire Wire Line
	10300 3150 10300 3250
Connection ~ 10300 3250
Wire Wire Line
	10300 3250 10300 3350
Connection ~ 10300 3350
Wire Wire Line
	10300 3350 10300 3450
Wire Wire Line
	10700 1950 10700 2050
Connection ~ 10700 2050
Wire Wire Line
	10700 2050 10700 2150
Connection ~ 10700 2150
Wire Wire Line
	10700 2150 10700 2250
Connection ~ 10700 2250
Wire Wire Line
	10700 2250 10700 2350
Connection ~ 10700 2350
Wire Wire Line
	10700 2350 10700 2450
Connection ~ 10700 2450
Wire Wire Line
	10700 2450 10700 2550
Connection ~ 10700 2550
Wire Wire Line
	10700 2550 10700 2650
Connection ~ 10700 2650
Wire Wire Line
	10700 2650 10700 2750
Connection ~ 10700 2750
Wire Wire Line
	10700 2750 10700 2850
Connection ~ 10700 2850
Wire Wire Line
	10700 2850 10700 2950
Connection ~ 10700 2950
Wire Wire Line
	10700 2950 10700 3050
Connection ~ 10700 3050
Wire Wire Line
	10700 3050 10700 3150
Connection ~ 10700 3150
Wire Wire Line
	10700 3150 10700 3250
Connection ~ 10700 3250
Wire Wire Line
	10700 3250 10700 3350
Connection ~ 10700 3350
Wire Wire Line
	10700 3350 10700 3450
$Comp
L power:+3.3V #PWR0101
U 1 1 608194E7
P 10700 1550
F 0 "#PWR0101" H 10700 1400 50  0001 C CNN
F 1 "+3.3V" H 10715 1723 50  0000 C CNN
F 2 "" H 10700 1550 50  0001 C CNN
F 3 "" H 10700 1550 50  0001 C CNN
	1    10700 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6081953E
P 10300 3550
F 0 "#PWR0102" H 10300 3300 50  0001 C CNN
F 1 "GND" H 10305 3377 50  0000 C CNN
F 2 "" H 10300 3550 50  0001 C CNN
F 3 "" H 10300 3550 50  0001 C CNN
	1    10300 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 3450 10300 3550
Connection ~ 10300 3450
Wire Wire Line
	10700 1950 10700 1550
Connection ~ 10700 1950
$Comp
L power:GND #PWR0103
U 1 1 6083055C
P 7700 3650
F 0 "#PWR0103" H 7700 3400 50  0001 C CNN
F 1 "GND" H 7705 3477 50  0000 C CNN
F 2 "" H 7700 3650 50  0001 C CNN
F 3 "" H 7700 3650 50  0001 C CNN
	1    7700 3650
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0104
U 1 1 608305C4
P 8200 1450
F 0 "#PWR0104" H 8200 1300 50  0001 C CNN
F 1 "+3.3V" H 8215 1623 50  0000 C CNN
F 2 "" H 8200 1450 50  0001 C CNN
F 3 "" H 8200 1450 50  0001 C CNN
	1    8200 1450
	1    0    0    -1  
$EndComp
NoConn ~ 8700 2150
NoConn ~ 9550 4900
Wire Wire Line
	6450 4900 6450 5000
Connection ~ 6450 5000
Wire Wire Line
	6450 5000 6450 5100
Connection ~ 6450 5100
Wire Wire Line
	6450 5100 6450 5200
Connection ~ 6450 5200
Wire Wire Line
	6450 5200 6450 5300
Connection ~ 6450 5300
Wire Wire Line
	6450 5300 6450 5400
Connection ~ 6450 5400
Wire Wire Line
	6450 5400 6450 5500
Connection ~ 6450 5500
Wire Wire Line
	6450 5500 6450 5600
Wire Wire Line
	6450 5600 6450 5700
Connection ~ 6450 5600
$Comp
L power:GND #PWR0105
U 1 1 6085A86F
P 6450 5700
F 0 "#PWR0105" H 6450 5450 50  0001 C CNN
F 1 "GND" H 6455 5527 50  0000 C CNN
F 2 "" H 6450 5700 50  0001 C CNN
F 3 "" H 6450 5700 50  0001 C CNN
	1    6450 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 2150 6450 2250
Connection ~ 6450 2250
Wire Wire Line
	6450 2250 6450 2350
Connection ~ 6450 2350
Wire Wire Line
	6450 2350 6450 2450
Connection ~ 6450 2450
Wire Wire Line
	6450 2450 6450 2550
Connection ~ 6450 2550
Wire Wire Line
	6450 2550 6450 2650
Connection ~ 6450 2650
Wire Wire Line
	6450 2650 6450 2750
Connection ~ 6450 2750
Wire Wire Line
	6450 2750 6450 2850
$Comp
L power:GND #PWR0106
U 1 1 6085DD7E
P 6450 3000
F 0 "#PWR0106" H 6450 2750 50  0001 C CNN
F 1 "GND" H 6455 2827 50  0000 C CNN
F 2 "" H 6450 3000 50  0001 C CNN
F 3 "" H 6450 3000 50  0001 C CNN
	1    6450 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3000 6450 2850
Connection ~ 6450 2850
$Comp
L power:GND #PWR0107
U 1 1 6086278F
P 7700 1800
F 0 "#PWR0107" H 7700 1550 50  0001 C CNN
F 1 "GND" H 7705 1627 50  0000 C CNN
F 2 "" H 7700 1800 50  0001 C CNN
F 3 "" H 7700 1800 50  0001 C CNN
	1    7700 1800
	-1   0    0    1   
$EndComp
Wire Wire Line
	7700 1800 7700 2050
Wire Wire Line
	8700 2050 8750 2050
Wire Wire Line
	8750 2050 8750 3850
Wire Wire Line
	8750 3850 8500 3850
Wire Wire Line
	8500 3850 8500 4800
Wire Wire Line
	8500 4800 8550 4800
Wire Wire Line
	7700 3050 6900 3050
Wire Wire Line
	6900 3050 6900 4100
Wire Wire Line
	6900 4100 6350 4100
Wire Wire Line
	6900 5800 6900 4100
Connection ~ 6900 4100
Wire Wire Line
	6800 6000 6800 4200
Wire Wire Line
	7700 3250 6800 3250
Wire Wire Line
	6800 3250 6800 4200
Connection ~ 6800 4200
Wire Wire Line
	9550 4800 9550 3900
$Comp
L power:GND #PWR0109
U 1 1 608D3008
P 8550 6400
F 0 "#PWR0109" H 8550 6150 50  0001 C CNN
F 1 "GND" H 8555 6227 50  0000 C CNN
F 2 "" H 8550 6400 50  0001 C CNN
F 3 "" H 8550 6400 50  0001 C CNN
	1    8550 6400
	1    0    0    -1  
$EndComp
Text Label 6450 3900 0    50   ~ 0
PISO_Data
Text Label 6450 4100 0    50   ~ 0
PISO_Latch
Text Label 6450 4200 0    50   ~ 0
PISO_Clock
$Comp
L Device:C C1
U 1 1 608338ED
P 8750 1650
F 0 "C1" V 8498 1650 50  0000 C CNN
F 1 "0.1uF" V 8589 1650 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 8788 1500 50  0001 C CNN
F 3 "~" H 8750 1650 50  0001 C CNN
	1    8750 1650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 60833A69
P 8900 1700
F 0 "#PWR0110" H 8900 1450 50  0001 C CNN
F 1 "GND" H 8905 1527 50  0000 C CNN
F 2 "" H 8900 1700 50  0001 C CNN
F 3 "" H 8900 1700 50  0001 C CNN
	1    8900 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 608448B7
P 9750 4350
F 0 "C2" V 9498 4350 50  0000 C CNN
F 1 "0.1uF" V 9589 4350 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 9788 4200 50  0001 C CNN
F 3 "~" H 9750 4350 50  0001 C CNN
	1    9750 4350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 6084491B
P 9900 4450
F 0 "#PWR0111" H 9900 4200 50  0001 C CNN
F 1 "GND" H 9905 4277 50  0000 C CNN
F 2 "" H 9900 4450 50  0001 C CNN
F 3 "" H 9900 4450 50  0001 C CNN
	1    9900 4450
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0112
U 1 1 60844B7F
P 9050 4200
F 0 "#PWR0112" H 9050 4050 50  0001 C CNN
F 1 "+3.3V" H 9065 4373 50  0000 C CNN
F 2 "" H 9050 4200 50  0001 C CNN
F 3 "" H 9050 4200 50  0001 C CNN
	1    9050 4200
	1    0    0    -1  
$EndComp
Text Label 4700 3900 0    50   ~ 0
PISO_Latch
Text Label 4700 4000 0    50   ~ 0
PISO_Clock
Text Label 4700 4100 0    50   ~ 0
PISO_Data
Text Label 4700 4200 0    50   ~ 0
LED_LowV
$Comp
L power:GND #PWR0116
U 1 1 608D7051
P 3300 3300
F 0 "#PWR0116" H 3300 3050 50  0001 C CNN
F 1 "GND" H 3305 3127 50  0000 C CNN
F 2 "" H 3300 3300 50  0001 C CNN
F 3 "" H 3300 3300 50  0001 C CNN
	1    3300 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 3250 3300 3300
$Comp
L power:+5V #PWR0117
U 1 1 608E0A9C
P 2300 3200
F 0 "#PWR0117" H 2300 3050 50  0001 C CNN
F 1 "+5V" H 2315 3373 50  0000 C CNN
F 2 "" H 2300 3200 50  0001 C CNN
F 3 "" H 2300 3200 50  0001 C CNN
	1    2300 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 3200 2300 3250
$Comp
L power:GND #PWR0118
U 1 1 608EAAFC
P 2800 2450
F 0 "#PWR0118" H 2800 2200 50  0001 C CNN
F 1 "GND" H 2805 2277 50  0000 C CNN
F 2 "" H 2800 2450 50  0001 C CNN
F 3 "" H 2800 2450 50  0001 C CNN
	1    2800 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 2400 2800 2450
Wire Wire Line
	2500 2150 2000 2150
Text Label 2100 2150 0    50   ~ 0
LED_LowV
$Comp
L Connector:Conn_01x03_Male J1
U 1 1 60908808
P 3650 1850
F 0 "J1" H 3623 1780 50  0000 R CNN
F 1 "LEDs" H 3623 1871 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3650 1850 50  0001 C CNN
F 3 "~" H 3650 1850 50  0001 C CNN
	1    3650 1850
	-1   0    0    1   
$EndComp
Wire Wire Line
	3450 1850 3100 1850
Wire Wire Line
	3100 1850 3100 2150
$Comp
L power:GND #PWR0119
U 1 1 6091D587
P 3400 2050
F 0 "#PWR0119" H 3400 1800 50  0001 C CNN
F 1 "GND" H 3405 1877 50  0000 C CNN
F 2 "" H 3400 2050 50  0001 C CNN
F 3 "" H 3400 2050 50  0001 C CNN
	1    3400 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0120
U 1 1 6091D5E6
P 3400 1650
F 0 "#PWR0120" H 3400 1500 50  0001 C CNN
F 1 "+5V" H 3415 1823 50  0000 C CNN
F 2 "" H 3400 1650 50  0001 C CNN
F 3 "" H 3400 1650 50  0001 C CNN
	1    3400 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 1650 3400 1750
Wire Wire Line
	3400 1750 3450 1750
Wire Wire Line
	3400 2050 3400 1950
Wire Wire Line
	3400 1950 3450 1950
$Comp
L Connector:Conn_01x03_Male J5
U 1 1 6084A3D4
P 1450 5650
F 0 "J5" H 1423 5580 50  0000 R CNN
F 1 "Power In" H 1423 5671 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1450 5650 50  0001 C CNN
F 3 "~" H 1450 5650 50  0001 C CNN
	1    1450 5650
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0121
U 1 1 6084A512
P 1450 5300
F 0 "#PWR0121" H 1450 5150 50  0001 C CNN
F 1 "+5V" H 1465 5473 50  0000 C CNN
F 2 "" H 1450 5300 50  0001 C CNN
F 3 "" H 1450 5300 50  0001 C CNN
	1    1450 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 6084A555
P 1650 5450
F 0 "#PWR0122" H 1650 5200 50  0001 C CNN
F 1 "GND" H 1655 5277 50  0000 C CNN
F 2 "" H 1650 5450 50  0001 C CNN
F 3 "" H 1650 5450 50  0001 C CNN
	1    1650 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 5350 1550 5450
Wire Wire Line
	1550 5350 1650 5350
Wire Wire Line
	1650 5350 1650 5450
Wire Wire Line
	1450 5450 1450 5300
$Comp
L Connector:Conn_01x04_Male J2
U 1 1 6089DDD0
P 7100 750
F 0 "J2" V 7160 891 50  0000 L CNN
F 1 "U4-4-7" V 7251 891 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7100 750 50  0001 C CNN
F 3 "~" H 7100 750 50  0001 C CNN
	1    7100 750 
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x04_Male J6
U 1 1 6089DFAB
P 7500 950
F 0 "J6" V 7560 1091 50  0000 L CNN
F 1 "U4-0-3" V 7651 1091 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7500 950 50  0001 C CNN
F 3 "~" H 7500 950 50  0001 C CNN
	1    7500 950 
	0    1    1    0   
$EndComp
Wire Wire Line
	6750 2150 7600 2150
Wire Wire Line
	6750 2350 7400 2350
Wire Wire Line
	6750 2550 7200 2550
Wire Wire Line
	6750 2650 7100 2650
Wire Wire Line
	6750 2750 7000 2750
Wire Wire Line
	6750 2850 6900 2850
Wire Wire Line
	6900 950  6900 2850
Connection ~ 6900 2850
Wire Wire Line
	6900 2850 7700 2850
Wire Wire Line
	7000 950  7000 2750
Connection ~ 7000 2750
Wire Wire Line
	7000 2750 7700 2750
Wire Wire Line
	7100 950  7100 2650
Connection ~ 7100 2650
Wire Wire Line
	7100 2650 7700 2650
Connection ~ 7200 2550
Wire Wire Line
	7200 2550 7700 2550
Wire Wire Line
	7300 1150 7300 2450
Wire Wire Line
	6750 2450 7300 2450
Connection ~ 7300 2450
Wire Wire Line
	7300 2450 7700 2450
Wire Wire Line
	7400 1150 7400 2350
Connection ~ 7400 2350
Wire Wire Line
	7400 2350 7700 2350
Wire Wire Line
	7500 1150 7500 2250
Wire Wire Line
	6750 2250 7500 2250
Connection ~ 7500 2250
Wire Wire Line
	7500 2250 7700 2250
Wire Wire Line
	7600 1150 7600 2150
Connection ~ 7600 2150
Wire Wire Line
	7600 2150 7700 2150
Wire Wire Line
	7200 950  7200 2550
Wire Wire Line
	6900 5800 8550 5800
Wire Wire Line
	6800 6000 8550 6000
Wire Wire Line
	6750 4900 8000 4900
Wire Wire Line
	6750 5000 7900 5000
Wire Wire Line
	6750 5100 7800 5100
Wire Wire Line
	6750 5200 7700 5200
Wire Wire Line
	6750 5300 7600 5300
Wire Wire Line
	6750 5400 7500 5400
Wire Wire Line
	6750 5500 7400 5500
Wire Wire Line
	6750 5600 7300 5600
$Comp
L Connector:Conn_01x04_Male J7
U 1 1 6095602D
P 7500 4050
F 0 "J7" V 7560 4191 50  0000 L CNN
F 1 "U3-4-7" V 7651 4191 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7500 4050 50  0001 C CNN
F 3 "~" H 7500 4050 50  0001 C CNN
	1    7500 4050
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x04_Male J8
U 1 1 60956033
P 7900 4250
F 0 "J8" V 7960 4391 50  0000 L CNN
F 1 "U3-0-3" V 8051 4391 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7900 4250 50  0001 C CNN
F 3 "~" H 7900 4250 50  0001 C CNN
	1    7900 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	8000 4450 8000 4900
Connection ~ 8000 4900
Wire Wire Line
	8000 4900 8550 4900
Wire Wire Line
	7900 4450 7900 5000
Connection ~ 7900 5000
Wire Wire Line
	7900 5000 8550 5000
Wire Wire Line
	7800 4450 7800 5100
Connection ~ 7800 5100
Wire Wire Line
	7800 5100 8550 5100
Wire Wire Line
	7700 4450 7700 5200
Connection ~ 7700 5200
Wire Wire Line
	7700 5200 8550 5200
Wire Wire Line
	7600 4250 7600 5300
Connection ~ 7600 5300
Wire Wire Line
	7600 5300 8550 5300
Wire Wire Line
	7500 4250 7500 5400
Connection ~ 7500 5400
Wire Wire Line
	7500 5400 8550 5400
Wire Wire Line
	7400 4250 7400 5500
Connection ~ 7400 5500
Wire Wire Line
	7400 5500 8550 5500
Wire Wire Line
	7300 4250 7300 5600
Connection ~ 7300 5600
Wire Wire Line
	7300 5600 8550 5600
$Comp
L Connector:Conn_01x04_Male J9
U 1 1 6099679C
P 4450 4000
F 0 "J9" H 4556 4278 50  0000 C CNN
F 1 "ESP_Connection" H 4556 4187 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4450 4000 50  0001 C CNN
F 3 "~" H 4450 4000 50  0001 C CNN
	1    4450 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 4200 6350 4200
Wire Wire Line
	6350 3900 9550 3900
Wire Wire Line
	4650 3900 5150 3900
Wire Wire Line
	4650 4000 5150 4000
Wire Wire Line
	4650 4100 5150 4100
Wire Wire Line
	4650 4200 5150 4200
Wire Wire Line
	8900 1650 8900 1700
Wire Wire Line
	8200 1450 8200 1650
Wire Wire Line
	8600 1650 8200 1650
Connection ~ 8200 1650
Wire Wire Line
	8200 1650 8200 1750
Wire Wire Line
	9050 4200 9050 4350
Wire Wire Line
	9600 4350 9050 4350
Connection ~ 9050 4350
Wire Wire Line
	9050 4350 9050 4500
Wire Wire Line
	9900 4350 9900 4450
$Comp
L power:+3.3V #PWR0113
U 1 1 608CE85A
P 1350 5300
F 0 "#PWR0113" H 1350 5150 50  0001 C CNN
F 1 "+3.3V" H 1365 5473 50  0000 C CNN
F 2 "" H 1350 5300 50  0001 C CNN
F 3 "" H 1350 5300 50  0001 C CNN
	1    1350 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 5300 1350 5450
Wire Wire Line
	7700 3350 7700 3650
Wire Wire Line
	7700 3650 8200 3650
Connection ~ 7700 3650
Wire Wire Line
	8550 6100 8550 6400
Wire Wire Line
	8550 6400 9050 6400
Connection ~ 8550 6400
$EndSCHEMATC
